<?php


print"<h1>Variables en PHP</h1>";


//Variable Numérico

print"<h2>Variable numérico</h2>";

$numero = 10;

echo 'Esto es un número: '.$numero; // (Sintaxis de como unir una cadena de texto con un numero)

echo '<br>';  //(Etiqueta Br funciona para dejar un espacio)

//Variable tipo texto

print"<h2>Variable tipo texto</h2>";

$texto = "Esto es una cadena de texto";

echo 'Mi Cadena: '.$texto;

//Variable booleana

print"<h2>Variable booleana</h2>";

$booleana = true; // (Pueden ser true o false)

echo 'Esto es una variable booleana: '.$booleana;

// Variable de tipo arreglo

print"<h2>Variable tipo arreglo</h2>";

$arreglo = array("Anthony", "Vizcarrondo"); //(Los datos se enumeran según sean ingresados en el arreglo)

echo $arreglo[0];

// Variable tipo arreglo con propiedad

print"<h2>Variable tipo arreglo con propiedad</h2>";

$colores = array ("color1"=>"rojo", "color2"=>"azul"); //(Asigna un "significado" a un componente del array)

echo $colores["color2"];

// Variable de tipo objeto

print"<h2>Variable tipo objeto</h2>";

$objeto = (object)["mueble1"=>"armario", "mueble2"=>"escritorio"];

echo $objeto->mueble2;

?>