<?php

print "<h1> Funciones en PHP </h1>";

//Función sin parametros

print"<h2>Función sin parametros</h2>";

function bienvenido(){

    echo "Bienvenido al curso de PHP";

}

bienvenido();

// Función con parametros


print"<h2>Función con parametros</h2>";

function saludar($hola){

    echo $hola;

}

saludar("Hola a todos");

// Función con retorno


print"<h2>Función con retorno</h2>";

function retorno($retornar){

    return $retornar;

}

echo retorno("Retornando la función");

?>