<?php


print"<h1>Condicionales en PHP</h1>";

$a = 10;
$b = 5;

if ($a > $b){ //(if se refiere a "Si estas variables cumplen la condición, ejecutaran la guiente acción")

    echo $a. " Es mayor que: ".$b;

}

elseif($a == $b){ //(Si no cumplen la condición de if, sucedera esto)

    echo $a. " Es igual que: ".$b;

} 

else{ //(Esto significa "sino", en caso que no se cumpla ninguna condición)

    echo $a. " Es menor que: ".$b;
}

?>